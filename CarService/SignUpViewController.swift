//
//  SignUpViewController.swift
//  CarService
//
//  Created by Igor Nalyvaiko on 02.03.17.
//  Copyright © 2017 Igor Nalyvaiko. All rights reserved.
//

import UIKit
import Parse

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var backToLoginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        signUpButton.layer.cornerRadius = 10
        backToLoginButton.layer.cornerRadius = 10
    }
    
    @IBAction func signUpNow(_ sender: UIButton) {
        let newUser = PFUser()
        newUser.username = userTextField.text!
        newUser.password = passwordTextField.text!
        newUser.email = emailTextField.text!
        newUser.signUpInBackground { (saved, error) in
            if saved {
                self.performSegue(withIdentifier: "backToLoginPage", sender: nil)
            }
            else if let errorMessage = error {
                print(errorMessage)
            }
        }
    }
    
    @IBAction func backToLoginPage(_ sender: UIButton) {
        self.performSegue(withIdentifier: "backToLoginPage", sender: nil)
    }

}
