//
//  BidTableViewCell.swift
//  CarService
//
//  Created by Igor Nalyvaiko on 19.02.17.
//  Copyright © 2017 Igor Nalyvaiko. All rights reserved.
//

import UIKit

class BidTableViewCell: UITableViewCell {
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var repairType: UILabel!
    @IBOutlet weak var car: UILabel!
    @IBOutlet weak var problemDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
