//
//  SheduleTableViewCell.swift
//  CarService
//
//  Created by Igor Nalyvaiko on 20.02.17.
//  Copyright © 2017 Igor Nalyvaiko. All rights reserved.
//

import UIKit

class SheduleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var timeLable: UILabel!
    @IBOutlet weak var maintenanceView: UIView!
    @IBOutlet weak var renewalView: UIView!
    @IBOutlet weak var maintenanceButton: UIButton!
    @IBOutlet weak var renewalButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
