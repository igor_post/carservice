//
//  ChatTableViewCell.swift
//  CarService
//
//  Created by Igor Nalyvaiko on 17.02.17.
//  Copyright © 2017 Igor Nalyvaiko. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {

    @IBOutlet weak var background: UIView!
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var message: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
