//
//  LoginViewController.swift
//  CarService
//
//  Created by Igor Nalyvaiko on 28.02.17.
//  Copyright © 2017 Igor Nalyvaiko. All rights reserved.
//

import UIKit
import Parse

class LoginViewController: UIViewController {
    
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        logInButton.layer.cornerRadius = 10
        signUpButton.layer.cornerRadius = 10
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    @IBAction func login(_ sender: UIButton) {
        PFUser.logInWithUsername(inBackground: userTextField.text!, password: passwordTextField.text!) { (user, error) in
            if user != nil {
                print("Current user: \(String(describing: PFUser.current()?.description))")
                self.performSegue(withIdentifier: "mainTabBar", sender: nil)
            }
            else {
                print(error!)
            }
        }
    }
    
    @IBAction func signUp(_ sender: UIButton) {
        self.performSegue(withIdentifier: "signUp", sender: nil)
    }

}
