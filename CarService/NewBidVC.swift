//
//  NewBidVC.swift
//  CarService
//
//  Created by Igor Nalyvaiko on 14.02.17.
//  Copyright © 2017 Igor Nalyvaiko. All rights reserved.
//

import UIKit
import Parse

class NewBidVC: UIViewController {

    @IBOutlet weak var recordDayLabel: UILabel!
    @IBOutlet weak var recordTimeLabel: UILabel!
    @IBOutlet weak var repairTypeLabel: UILabel!
    @IBOutlet weak var carModel: UITextField!
    @IBOutlet weak var carRegNumber: UITextField!
    @IBOutlet weak var problemDescription: UITextView!
    
    var repairType = 0
    var parseItem: PFObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if let sheduleDate = self.parseItem?["date"] as? Date {
            let recordDay = WorkingWithDates.getDateDescription(date: sheduleDate, format: "dd.MM.yyyy")
            let recordTime = WorkingWithDates.getDateDescription(date: sheduleDate, format: "HH:mm")
            let weekDayDescription = WorkingWithDates.getWeekDayDescription(date: sheduleDate)
            recordDayLabel.text = "День записи: \(recordDay), \(weekDayDescription)"
            recordTimeLabel.text = "Время записи: \(recordTime)"
        }
        repairTypeLabel.text = "Вид ремонта: \(repairType == 0 ? "ТО" : "ВР")"
        
        tabBarController?.tabBar.isHidden = true
        
    }
    
    @IBAction func sendBid(_ sender: UIBarButtonItem) {
        let newBid = PFObject(className: "Bid")
        newBid["user"] = PFUser.current()
        newBid["date"] = Date()
        newBid["renewal"] = repairType == 1
        newBid["carModel"] = carModel.text
        newBid["carRegNumber"] = carRegNumber.text
        newBid["problemDescription"] = problemDescription.text
        newBid.saveInBackground { (saved, error) in
            if saved {
                if let sheduleItem = self.parseItem {
                    let repairTypeName = self.repairType == 0 ? "maintenance" : "renewal"
                    sheduleItem.setObject(newBid, forKey: repairTypeName)
                    sheduleItem.saveInBackground()
                }
            }
            else {
                print(error!)
            }
        }
        _ = navigationController?.popViewController(animated: true)
    }
    

}
