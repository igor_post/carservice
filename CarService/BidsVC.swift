//
//  BidsVC.swift
//  CarService
//
//  Created by Igor Nalyvaiko on 14.02.17.
//  Copyright © 2017 Igor Nalyvaiko. All rights reserved.
//

import UIKit
import Parse

class BidsVC: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var bidsTableView: UITableView!
    
    var bids = [PFObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        bidsTableView.dataSource = self
        bidsTableView.rowHeight = UITableViewAutomaticDimension
        bidsTableView.estimatedRowHeight = 170
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateBidList()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bids.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let bidCell = bidsTableView.dequeueReusableCell(withIdentifier: "bidCell", for: indexPath) as! BidTableViewCell
        let bid = bids[indexPath.row]
        let carModel = bid["carModel"] as? String
        let carRegNumber = bid["carRegNumber"] as? String
        
        bidCell.date?.text = getDateDescription(date: bid["date"] as? Date)
        bidCell.problemDescription?.text = bid["problemDescription"] as? String
        bidCell.car?.text = "Модель: \(carModel!), гос. №: \(carRegNumber!)"
        bidCell.repairType?.text = bid["renewal"] as! Bool ? "ВР" : "ТО"
        
        return bidCell
    }
    
    func getDateDescription(date: Date?) -> String {
        var dateDescription = ""
        if let dateFromOpt = date {
            dateDescription = WorkingWithDates.getDateDescription(date: dateFromOpt, format: "dd.MM.yyyy")
        }
        return dateDescription
    }
    
    func updateBidList() {
        let query = PFQuery(className: "Bid")
        query.whereKey("user", equalTo: PFUser.current()!)
        query.order(byDescending: "date")
        query.findObjectsInBackground{ (objects, error) in
            if let objects = objects {
                if self.bids.count > 0 {
                    self.bids.removeAll()
                }
                for bid in objects {
                    self.bids.append(bid)
                }
                self.bidsTableView.reloadData()
                //if self.bids.count > 0 {
                //    self.bidsTableView.scrollToRow(at: IndexPath(row: self.bids.count-1, section: 0), at: .bottom, animated: true)
                //}
            }
            else {
                print(error!)
            }
        }
    }

}
