//
//  WorkingWithDates.swift
//  CarService
//
//  Created by Igor Nalyvaiko on 01.03.17.
//  Copyright © 2017 Igor Nalyvaiko. All rights reserved.
//

import UIKit
import Parse

class WorkingWithDates: NSObject {
    
    static func getDateDescription (date: Date?, format: String) -> String {
        var dateDescription = ""
        if let date = date {
            let dF = DateFormatter()
            dF.dateFormat = format
            dateDescription = dF.string(from: date)
        }
        return dateDescription
    }
    
    static func getBeginOfDay(date: Date) -> Date {
        let calendar = Calendar(identifier: .gregorian)
        var dC = calendar.dateComponents([.day, .month, .year], from: date)
        dC.hour = 0
        dC.minute = 0
        dC.timeZone = TimeZone.current
        return Calendar.current.date(from: dC)!
    }
    
    static func getEndOfDay(date: Date) -> Date {
        let calendar = Calendar(identifier: .gregorian)
        var dC = calendar.dateComponents([.day, .month, .year], from: date)
        dC.timeZone = TimeZone.current
        dC.hour = 0
        dC.minute = 0
        dC.hour = dC.hour! + 24
        return Calendar.current.date(from: dC)!
    }
    
    static func getWeekDayDescription (date: Date?) -> String {
        var weekDayDescription = ""
        if let date = date {
            let calendar = Calendar(identifier: .gregorian)
            let dC = calendar.dateComponents([.weekday], from: date)
            switch Int(dC.weekday!) {
            case 2:
                weekDayDescription = "пн."
            case 3:
                weekDayDescription = "вт."
            case 4:
                weekDayDescription = "ср."
            case 5:
                weekDayDescription = "чт."
            case 6:
                weekDayDescription = "пт."
            case 7:
                weekDayDescription = "сб."
            case 1:
                weekDayDescription = "вс."
            default:
                weekDayDescription = ""
            }
        }
        return weekDayDescription
    }
    
    static func fillMounthShedule() {
        
        for day in 1...15 {
        //for day in 16...30 {
        //for day in 16...31 {
            fillShedule(month: 6, day: day)
        }
        
        //        let calendar = Calendar(identifier: .gregorian)
        //        var dC = calendar.dateComponents([.day, .month, .year], from: currentDate)
        //        dC.hour = 0
        //        dC.minute = 0
        //        dC.timeZone = TimeZone.current
        //        let dateFrom = Calendar.current.date(from: dC)
        //        dC.day = 31
        //        let dateTo = Calendar.current.date(from: dC)
        //
        //        print("From date: \(dateFrom?.description)")
        //        print("To date: \(dateTo?.description)")
        //
        //        let query = PFQuery(className: "Shedule")
        //        query.whereKey("date", greaterThan: dateFrom!)
        //        query.whereKey("date", lessThan: dateTo!)
        //        query.order(byAscending: "date")
        //        query.limit = 10000
        //        query.findObjectsInBackground{ (objects, error) in
        //            if let objects = objects {
        //                for bid in objects {
        //                    print("Removing record with date: \(bid["date"]!)")
        //                    bid.deleteInBackground()
        //                }
        //            }
        //            else {
        //                print(error!)
        //            }
        //        }
    }
    
    static func fillShedule(month: Int, day: Int) {
        
        // Начало 9:15
        // Окончание 21:15
        // Шаг 30 мин
        var dC = DateComponents()
        dC.year = 2017
        dC.month = month
        dC.day = day
        dC.hour = 21
        dC.minute = 15
        dC.timeZone = TimeZone.current
        
        let endDate = Calendar.current.date(from: dC)
        
        dC.hour = 9
        var currentDate = Calendar.current.date(from: dC)
        
        while currentDate! < endDate! {
            currentDate = Calendar.current.date(from: dC)
            //print("Shedule: \(currentDate!)")
            let newShed = PFObject(className: "Shedule")
            newShed["date"] = currentDate
            newShed.saveInBackground()
            dC.minute = dC.minute! + 30
        }
    }

}
