//
//  CallTableViewController.swift
//  CarService
//
//  Created by Igor Nalyvaiko on 22.02.17.
//  Copyright © 2017 Igor Nalyvaiko. All rights reserved.
//

import UIKit
import Parse

class CallTableViewController: UITableViewController {
    
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var callBackButton: UIButton!
    @IBOutlet weak var callButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callBackButton.layer.cornerRadius = 10
        callButton.layer.cornerRadius = 10
    }

    @IBAction func regCallBack(_ sender: UIButton) {
        
        let newCallBack = PFObject(className: "CallBack")
        newCallBack["user"] = PFUser.current()
        newCallBack["date"] = Date()
        newCallBack["phoneNumber"] = phoneNumberTextField.text
        newCallBack.saveInBackground { (saved, error) in
            if saved {
                let alertController = UIAlertController(title: "Заказ обратного звонка", message: "Ваш заказ обратного звонка зарегистрирован. В течениие пятнадцати минут с Вами свяжется наш представитель.", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                    (result : UIAlertAction) -> Void in
                    print("OK")
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
            else {
                print(error!)
            }
        }
        
    }
    
    @IBAction func call(_ sender: UIButton) {
        let url = NSURL(string: "tel://1234567890")! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }
    

}
