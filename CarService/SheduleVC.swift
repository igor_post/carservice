//
//  SheduleVC.swift
//  CarService
//
//  Created by Igor Nalyvaiko on 15.02.17.
//  Copyright © 2017 Igor Nalyvaiko. All rights reserved.
//

import UIKit
import Parse

class SheduleVC: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var sheduleTableView: UITableView!
    
    var shedule = [PFObject]()
    var currentDate = Date()
    let exclamationMarkImage = UIImage(named: "ExclamationMark")

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        sheduleTableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let tabBarIsHidden = tabBarController?.tabBar.isHidden {
            if tabBarIsHidden {
                tabBarController?.tabBar.isHidden = false
            }
        }
        updateSheduleList()
        //WorkingWithDates.fillMounthShedule() // ВНИМАНИЕ!!! Заполняет расписание на сервере.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shedule.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sheduleItemCell = sheduleTableView.dequeueReusableCell(withIdentifier: "sheduleItemCell", for: indexPath) as! SheduleTableViewCell
        let sheduleItem = shedule[indexPath.row]
        sheduleItemCell.timeLable.text = WorkingWithDates.getDateDescription(date: sheduleItem["date"] as? Date, format: "HH:mm")
        roundCorners(vievs: [sheduleItemCell.maintenanceView, sheduleItemCell.renewalView])
        sheduleItemCell.maintenanceButton.setImage(nil, for: .normal)
        setColor(sheduleItem: sheduleItem, viev: sheduleItemCell.maintenanceView, itemName: "maintenance")
        setColor(sheduleItem: sheduleItem, viev: sheduleItemCell.renewalView, itemName: "renewal")
        return sheduleItemCell
    }
    
    func setColor(sheduleItem: PFObject, viev: UIView, itemName: String) {
        viev.backgroundColor = sheduleItem[itemName] != nil ? .red : .green
        if let cuttentRenewalBid = sheduleItem[itemName] as? PFObject {
            if cuttentRenewalBid["user"] as? PFUser == PFUser.current() {
                viev.backgroundColor = .yellow
            }
        }
    }
    
    func roundCorners(vievs: [UIView]) {
        for view in vievs {
            view.layer.cornerRadius = CGFloat(view.frame.size.height / 2)
        }
    }
    
    func setnavigationItemTitle() {
        let dateDescription = WorkingWithDates.getDateDescription(date: currentDate, format: "dd.MM.yyyy")
        let weekDayDescription = WorkingWithDates.getWeekDayDescription(date: currentDate)
        navigationItem.title = "Запись на \(dateDescription), \(weekDayDescription)"
    }
    
    func updateSheduleList() {
        setnavigationItemTitle()
        let dateFrom = WorkingWithDates.getBeginOfDay(date: currentDate)
        let dateTo = WorkingWithDates.getEndOfDay(date: currentDate)
        let query = performQuery(dateFrom: dateFrom, dateTo: dateTo)
        query.findObjectsInBackground{ (objects, error) in
            if let objects = objects {
                if self.shedule.count > 0 {
                    self.shedule.removeAll()
                }
                for bid in objects {
                    self.shedule.append(bid)
                }
                self.sheduleTableView.reloadData()
            }
            else {
                print(error!)
            }
        }
    }
    
    func performQuery(dateFrom: Date, dateTo: Date) -> PFQuery<PFObject> {
        let query = PFQuery(className: "Shedule")
        query.whereKey("date", greaterThanOrEqualTo: dateFrom)
        query.whereKey("date", lessThan: dateTo)
        query.order(byAscending: "date")
        query.includeKey("maintenance")
        query.includeKey("renewal")
        return query
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let targetView = segue.destination as? NewBidVC {
            if let sender = sender as? UIButton {
                if let cell = sender.superview?.superview?.superview as? SheduleTableViewCell {
                    if let indexPath = sheduleTableView.indexPath(for: cell) {
                        targetView.repairType = sender.tag
                        let currentShedule = shedule[indexPath.row]
                        targetView.parseItem = currentShedule
                    }
                }
            }
        }
    }
    
    @IBAction func maintenenseButtonPressed(_ sender: UIButton) {
        if let cell = sender.superview?.superview?.superview as? SheduleTableViewCell {
            if let indexPath = sheduleTableView.indexPath(for: cell) {
                let currentShedule = shedule[indexPath.row]
                if currentShedule["maintenance"] != nil {
                    timeIsBusy(title: "Заявка на ТО")
                }
                else {
                    performSegue(withIdentifier: "newBid", sender: sender)
                }
            }
        }
    }
    
    @IBAction func renewalButtonPressed(_ sender: UIButton) {
        if let cell = sender.superview?.superview?.superview as? SheduleTableViewCell {
            if let indexPath = sheduleTableView.indexPath(for: cell) {
                let currentShedule = shedule[indexPath.row]
                if currentShedule["renewal"] != nil {
                    timeIsBusy(title: "Заявка на ВР")
                }
                else {
                    performSegue(withIdentifier: "newBid", sender: sender)
                }
            }
        }
    }
    
    @IBAction func previousDate(_ sender: UIBarButtonItem) {
        changeDate(days: -1)
    }
    
    @IBAction func nextDate(_ sender: UIBarButtonItem) {
        changeDate(days: 1)
    }
    
    func timeIsBusy(title: String) {
        let alertController = UIAlertController(title: title, message: "К сожалению, выбранное Вами время уже занято!", preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("OK")
        }
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func changeDate(days: Int) {
        var dC = Calendar.current.dateComponents(in: TimeZone.current, from: currentDate)
        dC.day = dC.day! + days
        currentDate = dC.date!
        updateSheduleList()
    }

}
