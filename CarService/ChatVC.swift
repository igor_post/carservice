//
//  ChatVC.swift
//  CarService
//
//  Created by Igor Nalyvaiko on 14.02.17.
//  Copyright © 2017 Igor Nalyvaiko. All rights reserved.
//

import UIKit
import Parse

class ChatVC: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var sendButton: UIButton!
    
    var correspondence = [PFObject]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        chatTableView.dataSource = self
        chatTableView.rowHeight = UITableViewAutomaticDimension
        chatTableView.estimatedRowHeight = 170
        
        messageTextView.layer.cornerRadius = 10
        sendButton.layer.cornerRadius = 10
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    func handleKeyboardNotification(notification: Notification) {
        if let userInfo = notification.userInfo {
            let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            //animaton = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
            print("Keyboard frame height: \(keyboardFrame.height)")
            //messageTextView
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateChatList()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return correspondence.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let message = correspondence[indexPath.row]
        let cellId = message["incoming"] as! Bool ? "incomingMessageCell" : "outgoingMessageCell"
        let messageCell = chatTableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ChatTableViewCell
        
        messageCell.background.layer.cornerRadius = 10
        messageCell.message?.numberOfLines = 0
        messageCell.message?.text = message["text"] as? String
        messageCell.dateTime?.text = getDateDescription(date: message["date"] as? Date)
        
        return messageCell
    }
    
    func getDateDescription(date: Date?) -> String {
        var dateDescription = ""
        if let dateFromOpt = date {
            let messageDay = WorkingWithDates.getDateDescription(date: dateFromOpt, format: "dd.MM.yyyy")
            let messageTime = WorkingWithDates.getDateDescription(date: dateFromOpt, format: "HH:mm")
            dateDescription += "\(messageDay) \n \(messageTime)"
        }
        return dateDescription
    }
    
    func updateChatList() {
        let query = PFQuery(className: "ChatMessage")
        query.whereKey("user", equalTo: PFUser.current()!)
        query.findObjectsInBackground{ (objects, error) in
            if let objects = objects {
                if self.correspondence.count > 0 {
                    self.correspondence.removeAll()
                }
                for message in objects {
                    self.correspondence.append(message)
                }
                self.chatTableView.reloadData()
                if self.correspondence.count > 0 {
                    self.chatTableView.scrollToRow(at: IndexPath(row: self.correspondence.count-1, section: 0), at: .bottom, animated: true)
                }
            }
            else {
                print(error!)
            }
        }
    }
    
    @IBAction func sendMessage(_ sender: UIButton) {
        let message = PFObject(className: "ChatMessage")
        message["user"] = PFUser.current()
        message["date"] = Date()
        message["text"] = messageTextView.text
        message["incoming"] = false
        message.saveInBackground()
        messageTextView.text = ""
        correspondence.append(message)
        chatTableView.reloadData()
        chatTableView.scrollToRow(at: IndexPath(row: correspondence.count-1, section: 0), at: .bottom, animated: true)
    }

}
