//
//  EvacuatorVC.swift
//  CarService
//
//  Created by Igor Nalyvaiko on 14.02.17.
//  Copyright © 2017 Igor Nalyvaiko. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import Parse

class EvacuatorVC: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    
    let locationManager = CLLocationManager()
    var currentLocation = CLLocation()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            //locationManager.requestLocation()
        }
        
        /*
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString("Київ, Смоленська") { (placemarks, error) in
            if error != nil {
                print(error!)
                return
            }
            if let placemarks = placemarks {
                let placemark = placemarks[0]
                
                let annotation = MKPointAnnotation()
                annotation.title = "Київ"
                annotation.subtitle = "вул. Смоленська"
                
                if let location = placemark.location {
                    annotation.coordinate = location.coordinate
                    self.mapView.showAnnotations([annotation], animated: true)
                    self.mapView.selectAnnotation(annotation, animated: true)
                    self.currentLocation = location
                }
                
            }
        }
        */
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        let location = locations.first
        let span: MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01)
        let location2d: CLLocationCoordinate2D = CLLocationCoordinate2DMake((location?.coordinate.latitude)!, (location?.coordinate.longitude)!)
        let region: MKCoordinateRegion = MKCoordinateRegionMake(location2d, span)
        mapView.setRegion(region, animated: true)
        mapView.showsUserLocation = true
        
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location!) { (placemarks, error) in
            if let placeMarkAddr = placemarks?[0].addressDictionary {
                for key in placeMarkAddr {
                    print("\(key.key) \(key.value)")
                }
            }
        }
 
        
        /*
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        if let location = locations.first {
            print("Found user's location: \(location)")
            let annotation = MKPointAnnotation()
            annotation.title = "++"
            annotation.subtitle = "=="
            annotation.coordinate = location.coordinate
            self.mapView.showAnnotations([annotation], animated: true)
            self.mapView.selectAnnotation(annotation, animated: true)
            self.currentLocation = location
        }
        */
        
        locationManager.stopUpdatingLocation()
        
    }
    
    @IBAction func evacuateButtonPressed(_ sender: UIBarButtonItem) {
        let newTowTruck = PFObject(className: "TowTruck")
        newTowTruck["user"] = PFUser.current()
        newTowTruck["date"] = Date()
        newTowTruck["geoPoint"] = PFGeoPoint(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
        newTowTruck.saveInBackground { (saved, error) in
            if saved {
                let alertController = UIAlertController(title: "Вызов эвакуатора", message: "Ваш вызов эвакуатора зарегистрирован. В течениие пятнадцати минут с Вами свяжется наш представитель.", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                    (result : UIAlertAction) -> Void in
                    print("OK")
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
            else {
                print(error!)
            }
        }
    }
    
}
